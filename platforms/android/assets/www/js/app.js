function App() {}

// эффекты смены слайда
App.fade = new BackStack.FadeEffect();
App.slide_right = new BackStack.SlideEffect({direction: 'right'});
App.slide_left = new BackStack.SlideEffect({direction: 'left'});

App.view = false;

// стек слайдов
App.stackNavigator = false;

App.controller = false;

App.urlStack = [];

/**
 * Проверка интернет соединения
 * @returns {boolean}
 */
App.checkConnection = function () {
    if (window.navigator.connection.type == Connection.NONE) {
        $('#i_error').show();
        return false;
    }
    $('#i_error').hide();
    return true;
};

/**
 * Показать индикатор ожидания
 */
App.showWait = function(){
    $('#wait').show();
};

/**
 * Скрыть индикатор ожидания
 */
App.hideWait = function(){
    $('#wait').hide();
};

/**
 * Смена экрана с сохранением истории
 * @param view
 * @param effect
 */
App.nextView = function (view, effect) {
    if (App.stackNavigator.viewsStack.length > 0) {
        App.stackNavigator.pushView(view, {}, effect);
    } else {
        App.stackNavigator.pushView(view, {}, App.fade);
    }
};

/**
 * Замена экрана без истории
 * @param view
 * @param effect
 */
App.replaceView = function (view, effect) {
    if (App.stackNavigator.viewsStack.length > 0) {
        App.stackNavigator.replaceView(view, {}, effect);
    } else {
        App.stackNavigator.pushView(view, {}, App.fade);
    }
};

/**
 * События для кнопки назад
 */
App.back = function(){
    if(App.view && App.view.rs){
        App.view.save();
        return;
    }
    App.urlStack.pop();
    if(Backbone.history.location.hash == '!/' || Backbone.history.location.hash == '' || App.urlStack.length == 0){
        navigator.app.exitApp();
    }
    else{
        App.controller.navigate('#!/' + App.urlStack.pop(), {trigger: true, replace: true});
    }
};

App.ajax = function(url, options, context){

    if(!App.checkConnection()){
        return;
    }

    App.showWait();

    var success = options.success;
    var error = options.error;
    var fail = options.fail;

    options.success = function(result){
        success(result, context);
        App.hideWait();
    };

    options.error = function(res){
        App.hideWait();
        if(res.status == 403){
            TokenModel.cleareToken();
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return;
        }
        error(context);
    };

    options.fail = function(){
        fail(context);
        App.hideWait();
    };

    $.ajax(url, options);
};

/**
 * Сохранение данных
 * @param id
 * @param json
 */
App.cacheSave = function (id, json) {
    var cache = window.localStorage.getItem(localStorage.auth_token);
    if(cache == undefined){
        cache = {};
    } else {
        cache = JSON.parse(cache);
    }
    cache[id] = json;
    window.localStorage.setItem(localStorage.auth_token, JSON.stringify(cache));
};

/**
 * Проверка существования данных в кеше
 * @param id
 * @returns {boolean}
 */
App.cacheCheck = function (id) {
    var cache = window.localStorage.getItem(localStorage.auth_token);
    if(cache == undefined){
        cache = {};
    } else {
        cache = JSON.parse(cache);
    }
    return cache[id] != undefined;
};

/**
 * Получение данных из кеша
 * @param id
 */
App.cacheGet = function (id) {
    var cache = window.localStorage.getItem(localStorage.auth_token);
    if(cache == undefined){
        cache = {};
    } else {
        cache = JSON.parse(cache);
    }
    return cache[id];
};

/**
 * Получение даты в виде строки
 * @param date
 */
App.getStringDate = function(date){
    var day = date.getDay() < 10 ? '0'+date.getDay() : date.getDay();
    var month = date.getMonth() < 10 ? '0'+date.getMonth() : date.getMonth();
    var hours = date.getHours() < 10 ? '0'+date.getHours() : date.getHours();
    var minutes = date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes();
    //var seconds = date.getSeconds() < 10 ? '0'+date.getSeconds() : date.getSeconds();
    return date.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
};

App.init = function(){
    $(document).ready(function() {
        if (device.platform != 'android' && device.platform != 'Android'){
            $('#main').css('margin-top', '20px');
        }
        // запуск приложения после успешной инициализации
        if (!App.stackNavigator || !App.controller) {
            App.stackNavigator = new BackStack.StackNavigator({
                el: '#main'
            });
            App.controller = new MainController();
            Backbone.history.start();  // Запускаем HTML5 History push

            // сохраняем историю переходов
            Backbone.history.on('route', _.bind(function (router, route) {
                App.urlStack.push(route);
            }, this));
        }
    });
};

document.addEventListener("backbutton", App.back, false);
document.addEventListener("deviceready", App.init, false);