var MainController = Backbone.Router.extend({

    routes: {
        "": "init", // Пустой hash-тэг
        "!/": "init", // Начальная страница
        "!/register": "register", // регистрация
        "!/pin": "pin", // регистрация
        "!/forgot_password": "forgot_password", // регистрация
        "!/questionList": "questionList", // вопросы
        "!/answer/:id": "answer", // просмотр вопроса и ответа
        "!/todoList": "todoList", // to do
        "!/todo/:id": "todo", // просмотр to do
        "!/noteList": "noteList", // заметки
        "!/note/:id": "note", // просмотр заметки
        "!/journalList": "journalList", // дневник
        "!/journal/:id": "journal", // просмотр записи дневника
        "!/settings": "settings" // настройки
    },

    init: function(){
        if(TokenModel.isToken()){
            if(SettingsModel.isEnablePin()){
                App.controller.navigate("#!/pin", {trigger: true, replace: true});
                return false;
            }
            App.controller.navigate("#!/questionList", {trigger: true, replace: true});
            return false;
        }
        var loginView = new LoginView();
        App.replaceView(loginView, App.fade);
    },

    pin: function(){
        var pin = new PinView();
        App.replaceView(pin, App.fade);
    },

    register: function(){
        var registeView = new RegisterView();
        App.replaceView(registeView, App.fade);
    },

    questionList: function(){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var listView = new QuesionsView();
        App.replaceView(listView, App.fade);
    },

    forgot_password: function () {
        var v = new ForgotPasswordView();
        App.replaceView(v, App.fade);
    },

    answer: function(id){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var v = new AnswerView();
        App.view = v;
        v.setData(QuestionsModel.getQuestion(id));
        App.replaceView(v, App.fade);
    },

    todoList: function(){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var listView = new TodosView();
        App.view = listView;
        App.replaceView(listView, App.fade);
    },

    todo: function(id){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var v = new TodoPageView();
        v.setData(TodoModel.getTodoById(id));
        v.id = id;
        App.replaceView(v, App.fade);
    },

    noteList: function(){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var listView = new NotesView();
        App.view = listView;
        App.replaceView(listView, App.fade);
    },

    note: function(id){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var v = new NotePageView();
        v.setData(NoteModel.getNoteById(id));
        v.id = id;
        App.replaceView(v, App.fade);
    },

    journalList: function(){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var listView = new JournalsView();
        App.view = listView;
        App.replaceView(listView, App.fade);
    },

    journal: function(id){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var v = new JournalPageView();
        v.setData(JournalModel.getNoteById(id));
        v.id = id;
        App.replaceView(v, App.fade);
    },

    settings: function(){
        if(!TokenModel.isToken()){
            App.controller.navigate("#!/", {trigger: true, replace: true});
            return false;
        }
        var v = new SettingsView();
        App.nextView(v, App.fade);
    }
});