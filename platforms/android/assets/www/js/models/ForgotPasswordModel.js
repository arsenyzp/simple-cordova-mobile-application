var ForgotPasswordModel = function(){};

ForgotPasswordModel.url = '/index.php?r=api/default/reset';

ForgotPasswordModel.email = '';

ForgotPasswordModel.validate = function(){
    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    if(!validateEmail(ForgotPasswordModel.email)){
        return {error: true, message: "Не верный email"};
    }
    return {error: false, message: ""} ;
};

ForgotPasswordModel.forgot = function(success, error, context){
    App.ajax(ConfigModel.url + ForgotPasswordModel.url,
        {
            type: 'post',
            data: {email : ForgotPasswordModel.email},
            success: success,
            error: error,
            fail: error
        },
    context);
};