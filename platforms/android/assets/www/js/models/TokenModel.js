var TokenModel = {
    url: '/index.php?r=api/default/token',
    save: function (token){
        localStorage.auth_token = token;
    },
    getTokenById: function(id, success, error){
        App.ajax(ConfigModel.url + TokenModel.url, {
            type: 'post',
            data: {google_id: id},
            success: success,
            error: error,
            fail: error
        })
    },
    getToken: function(){
        return localStorage.auth_token;
    },
    cleareToken: function(){
        delete localStorage.auth_token;
    },
    isToken: function(){
        return (localStorage.auth_token && localStorage.auth_token != "") ? true : false;
    }

};