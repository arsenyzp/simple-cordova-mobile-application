var SettingsModel = {
    save: function(enable, pin){
        var settings = App.cacheGet('settings');
        if(settings == undefined){
            settings = {};
        } else {
            settings = JSON.parse(settings);
        }
        settings.enable = enable;
        if(pin != ""){
            settings.pin = pin;
        }
        App.cacheSave('settings', JSON.stringify(settings));
    },
    isEnablePin: function(){
        var settings = App.cacheGet('settings');
        if(settings == undefined){
            settings = {};
        } else {
            settings = JSON.parse(settings);
        }
        return settings.enable;
    },
    getPin: function(){
        var settings = App.cacheGet('settings');
        if(settings == undefined){
            settings = {};
        } else {
            settings = JSON.parse(settings);
        }
        return settings.pin;
    }
};