var JournalModel = {
    url: '/index.php?r=api/default/journal',
    data: {},
    save: function(id, answer, status, success, error, context){
        var answers = App.cacheGet('journals');
        var date = new Date();
        if(answers == undefined){
            answers = {};
        } else {
            answers = JSON.parse(answers);
        }
        answers[id] = {
            id: id,
            staus: status,
            text: answer,
            date: App.getStringDate(date)
        };
        App.cacheSave('journals', JSON.stringify(answers));
        App.ajax(ConfigModel.url + JournalModel.url, {
            data: {
                token: TokenModel.getToken(),
                date: answers[id].date,
                text: answers[id].text,
                status: answers[id].status,
                id: id
            },
            type: 'post',
            success: function(res){
                // сохранение
                var obj = JSON.parse(res);
                answers[obj.data.id] = {
                    id: obj.data.id,
                    staus: status,
                    text: answer,
                    date: App.getStringDate(date)
                };
                delete answers[0];
                App.cacheSave('journals', JSON.stringify(answers));
                success(res, context);
            },
            fail: error,
            error: error
        }, context);
    },

    getNoteById: function(id){

        if(id == 0){
            return {
                id: 0,
                text: "",
                status: 'init'
            };
        }

        var answers = App.cacheGet('journals');
        if(answers == undefined){
            return false;
        } else {
            try{
                answers = JSON.parse(answers);
            } catch (e){
                return false;
            }
            return answers[id];
        }
    },

    delNote: function(id, success, error, context){
        var answers = App.cacheGet('journals');
        if(answers == undefined){
            return false;
        }
        try{
            answers = JSON.parse(answers);
        } catch (e){
            return false;
        }
        answers[id].status = 'delete';
        if(answers[id]){
            App.ajax(ConfigModel.url + JournalModel.url, {
                type: 'post',
                data: {
                    token: TokenModel.getToken(),
                    id: id,
                    status: 'delete'
                },
                success: function(res){
                    delete answers[id];
                    App.cacheSave('notes', JSON.stringify(answers));
                    success(res, context);
                },
                error: error,
                fail: error
            }, context);
        }
        App.cacheSave('journals', JSON.stringify(answers));
    },

    fetch: function(success, error, context){
        var answers = App.cacheGet('journals');
        if(answers == undefined){
            error(context);
        }
        try{
            answers = JSON.parse(answers);
        } catch (e){
            error(context);
        }
        success(answers, context);
    }
};