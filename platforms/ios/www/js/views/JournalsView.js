var JournalsView = Backbone.View.extend({
    template: $('#template_journal_list').html(),

    snapper: false,

    infinity: false,

    id: 0,

    rs: false,

    events: {
        'touchstart .item_answer' : 'edit',
        'click .open-left':'open_menu',
        //'click .add_button':'addNote',
        //'touchstart .remove_todo':'delNote',
        'click .back': 'back'
    },

    edit: function(ev){
        var id = $(ev.currentTarget).data('id');
        $('.item_answer').css('color', '#fff');
        $(ev.currentTarget).css('color', '#cecece');
        var answer = JournalModel.getNoteById(id);
        if(!answer)
            return false;
        this.id = id;
        this.$el.find('textarea').val(answer.text);
        this.$el.find('textarea').focus();
    },

    back: function(){
       // this.save();
    },

    save: function(){
        this.$el.find('textarea').data('open', false);
        if(this.$el.find('textarea').val() == ""){
            if(this.id !=0){
                this.delNote(this.id);
                this.id = 0;
            }
            return;
        }
        var answer = this.$el.find('textarea').val();
        var status = "init";
        JournalModel.save(this.id, answer, status, this.successSave, this.errorSave, this);
        this.id = 0;
    },
    successSave: function(res, context){
        context.showInfo("Успешно сохранено");
        context.id = 0;
        context.$el.find('textarea').val("");
        context.infinity.remove();
        context.infinity = new window.infinity.ListView(context.$el.find('.infinity'));
        JournalModel.fetch(context.renderList, context.renderError, context);
    },
    errorSave:function(context){
        context.showInfo("Не сохранено, проверьте соединение.");
    },

    delNote: function(id){
        //ev.stopPropagation();
        //var id = $(ev.currentTarget).parent().data('id');
        JournalModel.delNote(id, this.successDelete, this.errorDelete, this);
        this.infinity.remove();
        this.infinity = new window.infinity.ListView(this.$el.find('.infinity'));
        JournalModel.fetch(this.renderList, this.renderError, this);
    },

    successDelete: function(res, context){
        context.showInfo("Удалено.");
    },

    errorDelete: function(context){
        context.showInfo("Удалено, но не удалено с сервера, проверьте соединение.");
    },

    showInfo: function(info){
        this.$el.find('.showInfoPopup').text(info);
        this.$el.find('.showInfoPopup').show();
        var self = this;
        setTimeout(function(){
            self.$el.find('.showInfoPopup').hide();
        }, 2000);
    },

    addNote: function(ev){
        App.controller.navigate("#!/journal/0", {trigger: true, replace: true});
    },

    //openNote: function(ev){
    //    var id = $(ev.currentTarget).data('id');
    //    App.controller.navigate("#!/journal/"+id, {trigger: true, replace: true});
    //},

    open_menu: function(ev){
        this.snapper.open('left');
        ev.stopPropagation();
    },

    renderList: function(data, context){
        //отображение списка
        if(data){
            for(var i in data){
                if(data[i].text == "" || data[i].text == undefined){
                    continue;
                }
                if(data[i].status == "delete"){
                    continue;
                }
                var v = new JournalItemView();
                context.infinity.prepend(v.render({
                    id: i,
                    text: data[i].text,
                    date: data[i].date
                }).$el);
            }
        }
    },

    renderError: function(message){

    },

    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({}));
        this.infinity = new window.infinity.ListView(this.$el.find('.infinity'));
        JournalModel.fetch(this.renderList, this.renderError, this);

        this.snapper = new Snap({
            element: this.$el.find('.content')[0],
            maxPosition: 266,
            minPosition: 0
        });

        var self = this;
        this.$el.find('textarea').on('blur', function(){
            self.save();
            $(this).css('height', '50px');
            $(this).css('top', 'auto');
            self.$el.find('.back').hide();
            self.$el.find('.open-left').show();
            self.rs = false;
        });
        this.$el.find('textarea').on('focus', function(){
            $(this).css('height', 'auto');
            $(this).css('top', '50px');
            $(this).data('open', true);
            self.$el.find('.back').show();
            self.$el.find('.open-left').hide();
            self.rs = true;
        });

        return this;
    }
});


// App.controller.navigate("#!/forgot_password", {trigger: true, replace: true});