var LoginView = Backbone.View.extend({
    template: $('#template_login').html(),
    events: {
        'touchstart .app_button':'login',
        'touchstart .googleLogin':'googleAuth'
    },

    login: function(ev){
        LoginModel.email = this.$el.find('input[name="email"]').val();
        LoginModel.password = this.$el.find('input[name="password"]').val();
        var validate = LoginModel.validate();
        if(validate.error){
            this.$el.find('#error').text(validate.message);
            return false;
        }
        LoginModel.login(this.successLogin, this.errorLogin);
    },

    successLogin: function(res){
        var obj = JSON.parse(res);
        if(obj.error){
            var text_error = "";
            for(var e in obj.error){
                text_error += obj.error[e];
            }
            $('#error').text(text_error);
        } else {
            //сохраяем полученый токен
            TokenModel.save(obj.token);

            // синхронизация
            SyncHelper.sync(function(){
                App.controller.navigate("#!/questionList", {trigger: true, replace: true});
            },function(){
                App.controller.navigate("#!/questionList", {trigger: true, replace: true});
            })


        }
    },

    errorLogin: function(){
        $('#error').text('Ошибка работы сервера');
    },

    googleAuth: function(){
        if(!App.checkConnection()){
            return;
        }
        var self = this;
        //Show the consent page
        GoogleModel.authorize({
            client_id: GoogleModel.client_id,
            client_secret: GoogleModel.client_secret,
            redirect_uri: GoogleModel.redirect_uri,
            scope: GoogleModel.scope
        }).done(function() {
            //Show the greet view if access is granted
            GoogleModel.getToken({
                client_id: GoogleModel.client_id,
                client_secret: GoogleModel.client_secret
            }).then(function(data) {
                //Pass the token to the API call and return a new promise object
                return GoogleModel.userInfo({ access_token: data.access_token });
            }).done(function(user) {
                //Display a greeting if the API call was successful
                console.log(user);

                TokenModel.getTokenById(user.id, self.successLogin, self.errorLogin);
            }).fail(function() {
                //If getting the token fails, or the token has been
                //revoked, show the login view.
                App.controller.navigate("#!/", {trigger: true, replace: true});
            });
        }).fail(function(data) {
            //Show an error message if access was denied
            $('#error').html(data.error);
        });
    },

    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({}));
        return this;
    }
});


// App.controller.navigate("#!/forgot_password", {trigger: true, replace: true});