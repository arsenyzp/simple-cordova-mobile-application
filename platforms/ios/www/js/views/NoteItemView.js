var NoteItemView = Backbone.View.extend({
    template: $('#template_note_item').html(),
    events: {

    },
    render: function (json) {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl(json));
        return this;
    }
});