var SettingsView = Backbone.View.extend({
    template: $('#template_settings_page').html(),
    snapper: false,
    events: {
        'click .save': 'save',
        'click .exit': 'exit',
        'click .open-left':'open_menu'
    },
    open_menu: function(ev){
        this.snapper.open('left');
        ev.stopPropagation();
    },
    save: function(){
        var enable_pin = this.$el.find('#pin').prop('checked');
        var pin1 = this.$el.find('#pin1').val();
        var pin2 = this.$el.find('#pin2').val();
        if(pin1 != pin2){
            this.showInfo("Pin не совпадают");
            return;
        }
        SettingsModel.save(enable_pin, pin1);
        this.showInfo("Настройки сохранены");
    },
    exit: function(){
        TokenModel.cleareToken();
        App.controller.navigate("#!/", {trigger: true, replace: true});
    },
    showInfo: function(info){
        this.$el.find('.showInfoPopup').text(info);
        this.$el.find('.showInfoPopup').show();
        var self = this;
        setTimeout(function(){
            self.$el.find('.showInfoPopup').hide();
        }, 2000);
    },
    render: function () {
        var tmpl = _.template(this.template);
        var enable_pin = SettingsModel.isEnablePin() ? 'checked="checked"' : '';
        this.$el.html(tmpl({enable_pin: enable_pin}));

        this.snapper = new Snap({
            element: this.$el.find('.content')[0],
            maxPosition: 266,
            minPosition: 0
        });

        return this;
    }
});