var PinView = Backbone.View.extend({
    template: $('#template_pin_page').html(),
    events: {
        'touchstart .app_button' : 'pin'
    },
    pin: function(){
        var pin = this.$el.find('#pin_check').val();
        if(pin != SettingsModel.getPin()){
            this.$el.find('#error').text("Не верный pin");
            return false;
        }
        App.controller.navigate("#!/questionList", {trigger: true, replace: true});
    },
    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({}));
        return this;
    }
});