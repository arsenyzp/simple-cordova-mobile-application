var SyncHelper = {
    sync: function(success, error){
        App.ajax(ConfigModel.url + '/index.php?r=api/default/stored-data',{
            type: 'post',
            data: {token: TokenModel.getToken()},
            success:function(res){
                var obj = JSON.parse(res);
                if(obj.data.answers){
                    var data = {};
                    for(var i in obj.data.answers){
                        if(data[obj.data.answers[i].id_question] == undefined){
                            data[obj.data.answers[i].id_question] = [];
                        }
                        data[obj.data.answers[i].id_question].push({
                            text:obj.data.answers[i].answer,
                            id:obj.data.answers[i].id,
                            id_question:obj.data.answers[i].id_question,
                            date:obj.data.answers[i].date_answer
                        });
                    }
                    App.cacheSave('answers', JSON.stringify(data));
                }
                if(obj.data.journals){
                    var data = {};
                    for(var i in obj.data.journals){
                        data[obj.data.journals[i].id] ={
                            id: obj.data.journals[i].id,
                            text:obj.data.journals[i].text,
                            date:obj.data.journals[i].date_update,
                            status: 'init'
                        };
                    }
                    App.cacheSave('journals', JSON.stringify(data));
                }
                if(obj.data.notes){
                    var data = {};
                    for(var i in obj.data.notes){
                        data[obj.data.notes[i].id] ={
                            id: obj.data.notes[i].id,
                            text:obj.data.notes[i].text,
                            date:obj.data.notes[i].date_update,
                            status: 'init'
                        };
                    }
                    App.cacheSave('notes', JSON.stringify(data));
                }
                if(obj.data.todos){
                    var data = {};
                    for(var i in obj.data.todos){
                        data[obj.data.todos[i].id] ={
                            id: obj.data.todos[i].id,
                            text:obj.data.todos[i].text,
                            date:obj.data.todos[i].date_update,
                            status: obj.data.todos[i].status
                        };
                    }
                    App.cacheSave('todos', JSON.stringify(data));
                }
                success(res);
            },
            error: error,
            fail: error
        });
    },
    send: function(){

    }
};