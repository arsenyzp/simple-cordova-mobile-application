var LoginModel = function(){};

LoginModel.url = '/index.php?r=api/default/login';

LoginModel.email = "";
LoginModel.password = "";

LoginModel.validate = function(){
    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    if(!validateEmail(LoginModel.email)){
        return {error: true, message: "Не верный email"};
    }
    if(LoginModel.password == ""){
        return {error: true, message: "Забыли ввести пароль"};
    }
    return {error: false, message: ""} ;
};

LoginModel.login = function(success, error){
    App.ajax(ConfigModel.url + LoginModel.url, {
        type: 'post',
        data: {email: LoginModel.email, password: LoginModel.password},
        success: success,
        error: error,
        fail: error
    })
};