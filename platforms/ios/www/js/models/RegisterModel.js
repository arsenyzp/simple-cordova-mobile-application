var RegisterModel = function(){};

RegisterModel.url = '/index.php?r=api/default/register';

RegisterModel.email = "";
RegisterModel.password = "";
RegisterModel.confirm_password = "";

RegisterModel.validate = function(){
    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
    if(!validateEmail(RegisterModel.email)){
        return {error: true, message: "Не верный email"};
    }
    if(RegisterModel.password == ""){
        return {error: true, message: "Забыли ввести пароль"};
    }
    if(RegisterModel.password != RegisterModel.confirm_password){
        return {error: true, message: "Пароли не совпадают"};
    }
    return {error: false, message: ""} ;
};

RegisterModel.register = function(success, error){
    App.ajax(ConfigModel.url + RegisterModel.url, {
        type: 'post',
        data: {email: RegisterModel.email, password: RegisterModel.password, confirm_password: RegisterModel.confirm_password},
        success: success,
        error: error,
        fail: error
    })
};