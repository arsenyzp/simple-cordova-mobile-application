var QuestionsModel = {
    url : '/index.php?r=api/default/questions',
    data: {},
    // получние данных
    fetch: function(success, error, context){
        App.ajax(ConfigModel.url + QuestionsModel.url,{
            data: {token: TokenModel.getToken()},
            type: 'post',
            success: function(res){
                var obj = JSON.parse(res);
                if(!obj.error){
                    if(obj.data.length >0 ){
                        QuestionsModel.data = obj.data;
                        success(QuestionsModel.data, context);
                    } else { // нет вопросов

                    }
                } else { // ошибки
                    error(obj.error, context);
                }
            },
            error: error,
            fail: error
        })
    },
    // сохранение в кеше
    cache: function(data){

    },

    getQuestion: function(id){
        for(var i in QuestionsModel.data){
            if(QuestionsModel.data[i].id == id){
                return QuestionsModel.data[i];
            }
        }
        return false;
    },

    getFirstId: function(){

        if(App.cacheCheck('last_question')){
            return QuestionsModel.getNextId(App.cacheGet('last_question'));
        }

        if(QuestionsModel.data[0])
            return QuestionsModel.data[0].id;
        else
            return false;
    },

    getNextId: function(id){
        var ret = false
        for(var i in QuestionsModel.data){
            if(ret){
                return QuestionsModel.data[i].id;
            }
            if(QuestionsModel.data[i].id == id){
                App.cacheSave('last_question', id);
                ret = true;
            }
        }
        return false;
    }
};