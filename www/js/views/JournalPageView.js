var JournalPageView = Backbone.View.extend({
    template: $('#template_journal_page').html(),
    data: {},
    id: 0,
    snapper: false,
    setData: function(data){
        this.data = data;
    },
    events: {
        'click .save': 'save',
        'click .back': 'back'
    },
    back: function(){
        App.controller.navigate("#!/journalList", {trigger: true, replace: true});
    },
    save: function(){
        var answer = this.$el.find('textarea').val();
        var status = "init";
        JournalModel.save(this.id, answer, status, this.successSave, this.errorSave, this);
    },
    successSave: function(res, context){
        context.showInfo("Успешно сохранено");
        setTimeout(function(){
            App.controller.navigate("#!/journalList", {trigger: true, replace: true});
        }, 1000);
    },
    errorSave:function(context){
        context.showInfo("Не сохранено, проверьте соединение.");
    },
    showInfo: function(info){
        this.$el.find('.showInfoPopup').text(info);
        this.$el.find('.showInfoPopup').show();
        var self = this;
        setTimeout(function(){
            self.$el.find('.showInfoPopup').hide();
        }, 2000);
    },
    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl(this.data));
        return this;
    }
});