var ForgotPasswordView = Backbone.View.extend({
    template: $('#template_forgot_password').html(),
    events: {
        'touchstart .app_button':'forgot',
        'touchstart .googleLogin':'googleAuth'
    },

    forgot: function(ev){
        ForgotPasswordModel.email = this.$el.find('input[name="email"]').val();
        var validate = ForgotPasswordModel.validate();
        if(validate.error){
            this.$el.find('#error').text(validate.message);
            return false;
        }
        ForgotPasswordModel.forgot(this.successForgot, this.errorForgot, this);
    },

    successForgot: function(res, context){
        res = JSON.parse(res);
        if(res.error){
            context.$el.find('#error').text(res.error);
            return false;
        }
        context.$el.find('#error').text("Пароль отправлен вам на почту");
    },

    errorForgot: function(context){
        context.$el.find('#error').text("Ошибка сервера");
    },

    googleAuth: function(ev){

    },

    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({}));
        return this;
    }
});


// App.controller.navigate("#!/forgot_password", {trigger: true, replace: true});