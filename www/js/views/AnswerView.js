var AnswerView = Backbone.View.extend({
    template: $('#template_answer').html(),
    template_answer_item: $('#template_answer_item').html(),
    data: {},
    id: 0,
    infinity: false,
    snapper: false,
    rs: false,
    setData: function(data){
        this.data = data;
    },
    events: {
        'click .save': 'save',
        'click .item_answer': 'edit',
   //     'click .open-left':'open_menu',
        'click .back': 'back'
    },
    //open_menu: function(ev){
    //    this.snapper.open('left');
    //    ev.stopPropagation();
    //},
    back: function(){
        if(this.$el.find('textarea').data('open')){
            this.save();
            return;
        }
        App.controller.navigate("#!/questionList", {trigger: true, replace: true});
    },
    save: function(){
        this.$el.find('textarea').data('open', false);
        this.$el.find('textarea').blur();
        if(this.$el.find('textarea').val() == ""){
            return;
        }
        var answer = {text: this.$el.find('textarea').val(), id: this.id};
        AnswerModel.save(this.data.id, answer, this.successSave, this.errorSave, this);
    },
    edit: function(ev){
        AnswerModel.play = false;
        var id = $(ev.currentTarget).data('id');
        $('.item_answer').css('color', '#fff');
        $(ev.currentTarget).css('color', '#cecece');
        var answer = AnswerModel.getIAnswerById(this.data.id, id);
        if(!answer)
            return false;
        this.id = id;
        this.$el.find('textarea').val(answer.text);
        this.$el.find('textarea').focus();
    },
    successSave: function(res, context){
        context.showInfo("Успешно сохранено");
        context.id = 0;
        if(AnswerModel.play){
            var id = QuestionsModel.getNextId(context.data.id);
            if(id ){
                AnswerModel.count++;
                App.controller.navigate("#!/answer/"+id, {trigger: true, replace: true});
                return;
            }
        }

        context.$el.find('textarea').val("");
        context.infinity.remove();
        context.infinity = new window.infinity.ListView(context.$el.find('.infinity'));
        context.render_answers();
    },
    errorSave:function(context){
        context.showInfo("Ошибка сохранения. Проверьте подключение");

        //if(AnswerModel.play && AnswerModel.count < 5){
        //    var id = QuestionsModel.getNextId(context.data.id);
        //    if(id ){
        //        AnswerModel.count++;
        //        App.controller.navigate("#!/answer/"+id, {trigger: true, replace: true});
        //        return;
        //    }
        //}
        //
        //context.$el.find('textarea').val("");
        //context.infinity.remove();
        //context.infinity = new window.infinity.ListView(context.$el.find('.infinity'));
        //context.render_answers();
    },
    showInfo: function(info){
        this.$el.find('.showInfoPopup').text(info);
        this.$el.find('.showInfoPopup').show();
        var self = this;
        setTimeout(function(){
            self.$el.find('.showInfoPopup').hide();
        }, 2000);
    },
    render_answers: function(){
        var answers = AnswerModel.getAnswerById(this.data.id);
        this.data.answers = [];
        if(answers && answers != undefined){
            this.data.answers = answers;
        }
        this.data.answers.reverse();
        for(var i in this.data.answers){
            var tmpl = _.template(this.template_answer_item);
            this.infinity.append(tmpl(this.data.answers[i]));
        }
    },
    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({text: this.data.text, id: this.data.id}));
        this.infinity = new window.infinity.ListView(this.$el.find('.infinity'));
        this.render_answers();

        //this.snapper = new Snap({
        //    element: this.$el.find('.content')[0],
        //    maxPosition: 266,
        //    minPosition: 0
        //});

        var self = this;
        this.$el.find('textarea').on('blur', function(){
            $(this).css('height', '50px');
            $(this).css('top', 'auto');
            self.rs = false;
        });
        this.$el.find('textarea').on('focus', function(){
            $(this).css('height', 'auto');
            $(this).css('top', '50px');
            $(this).data('open', true);
            self.rs = true;
        });
        return this;
    }
});