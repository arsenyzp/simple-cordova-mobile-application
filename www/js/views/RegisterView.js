var RegisterView = Backbone.View.extend({
    template: $('#template_register').html(),
    events: {
        'touchstart .app_button':'register',
        'touchstart .googleLogin':'googleAuth'
    },
    register: function(ev){
        RegisterModel.email = this.$el.find('input[name="email"]').val();
        RegisterModel.password = this.$el.find('input[name="password"]').val();
        RegisterModel.confirm_password = this.$el.find('input[name="password_confirm"]').val();
        var validate = RegisterModel.validate();
        if(validate.error){
            this.$el.find('#error').text(validate.message);
            return false;
        }
        RegisterModel.register(this.successRegister, this.errorRegister);
    },
    successRegister: function(res){
        var obj = JSON.parse(res);
        if(obj.error){
            var text_error = "";
            for(var e in obj.error){
                text_error += obj.error[e];
            }
            $('#error').text(text_error);
        } else {
            //сохраяем полученый токен
            TokenModel.save(obj.token);
            App.controller.navigate("#!/questionList", {trigger: true, replace: true});
        }
    },
    errorRegister: function(){
        $('#error').text('Ошибка работы сервера');
    },
    googleAuth: function(){
        var self = this;
        //Show the consent page
        GoogleModel.authorize({
            client_id: GoogleModel.client_id,
            client_secret: GoogleModel.client_secret,
            redirect_uri: GoogleModel.redirect_uri,
            scope: GoogleModel.scope
        }).done(function() {
            //Show the greet view if access is granted
            GoogleModel.getToken({
                client_id: GoogleModel.client_id,
                client_secret: GoogleModel.client_secret
            }).then(function(data) {
                //Pass the token to the API call and return a new promise object
                return GoogleModel.userInfo({ access_token: data.access_token });
            }).done(function(user) {
                //Display a greeting if the API call was successful
                console.log(user);

                TokenModel.getTokenById(user.id, self.successRegister, self.errorRegister);
            }).fail(function() {
                //If getting the token fails, or the token has been
                //revoked, show the login view.
                App.controller.navigate("#!/", {trigger: true, replace: true});
            });
        }).fail(function(data) {
            //Show an error message if access was denied
            $('#error').html(data.error);
        });
    },
    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({}));
        return this;
    }
});