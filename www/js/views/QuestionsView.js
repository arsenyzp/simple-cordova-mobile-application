var QuesionsView = Backbone.View.extend({
    template: $('#template_questions_list').html(),

    snapper: false,

    infinity: false,

    events: {
        'click .item_questions' : 'openQuestion',
        'click .open-left':'open_menu',
        'click .play':'play'
    },

    play: function(){
        AnswerModel.play = true;
        AnswerModel.count = 1;
        var id = QuestionsModel.getFirstId();
        if(!id){
            return false;
        }
        App.controller.navigate("#!/answer/"+id, {trigger: true, replace: true});
    },

    openQuestion: function(ev){
        AnswerModel.play = false;
        var id = $(ev.currentTarget).data('id');
        App.controller.navigate("#!/answer/"+id, {trigger: true, replace: true});
    },

    open_menu: function(ev){
        this.snapper.open('left');
        ev.stopPropagation();
    },

    renderList: function(data, context){
        //отображение картинок
        if(data){
            for(var i in data){
                var v = new QuestionItemView();
                context.infinity.append(v.render({
                    id: data[i].id,
                    text: data[i].text
                }).$el);
            }
        }
    },

    showInfo: function(info){
        this.$el.find('.showInfoPopup').text(info);
        this.$el.find('.showInfoPopup').show();
        var self = this;
        setTimeout(function(){
            self.$el.find('.showInfoPopup').hide();
        }, 2000);
    },

    renderError: function(context){
        context.showInfo("Проверьте подключение и повторите попытку.");
    },

    render: function () {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl({}));
        this.infinity = new window.infinity.ListView(this.$el.find('.infinity'));
        QuestionsModel.fetch(this.renderList, this.renderError, this);

        this.snapper = new Snap({
            element: this.$el.find('.content')[0],
            maxPosition: 266,
            minPosition: 0
        });

        return this;
    }
});


// App.controller.navigate("#!/forgot_password", {trigger: true, replace: true});