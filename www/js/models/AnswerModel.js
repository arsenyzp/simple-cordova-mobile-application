var AnswerModel = {
    url: '/index.php?r=api/default/answer',
    data: {},
    play: false,
    count: 1,
    save: function(id_question, answer, success, error, context){
        var answers = App.cacheGet('answers');
        var date = new Date();
        if(answers == undefined){
            answers = {};
        } else {
            answers = JSON.parse(answers);
        }
        if(answers[id_question] == undefined){
            answers[id_question] = [];
        }
        //answers[id_question].push({
        //    id: 0,
        //    id_question: id_question,
        //    text:answer,
        //    date: App.getStringDate(date)
        //});
        //App.cacheSave('answers', JSON.stringify(answers));
        App.ajax(ConfigModel.url + AnswerModel.url, {
            data: {
                id:answer.id,
                token: TokenModel.getToken(),
                date: App.getStringDate(date),
                text: answer.text,
                id_question: id_question
            },
            type: 'post',
            success: function(res){
                var obj = JSON.parse(res);
                // заменяем старую запись
                if(answer.id != 0)
                    for(var i in answers[id_question]){
                        if(answers[id_question][i].id == obj.data.id){
                            answers[id_question][i] = {
                                id: obj.data.id,
                                id_question: id_question,
                                text:answer.text,
                                date: App.getStringDate(date)
                            };
                        }
                    }
                else // добавляем новую
                    answers[id_question].push({
                        id: obj.data.id,
                        id_question: id_question,
                        text:answer.text,
                        date: App.getStringDate(date)
                    });
                App.cacheSave('answers', JSON.stringify(answers));
                success(res, context);
            },
            fail: error,
            error: error
        }, context);
    },

    getAnswerById: function(id){
        var answers = App.cacheGet('answers');
        if(answers == undefined){
            return false;
        } else {
            try{
                answers = JSON.parse(answers);
            } catch (e){
                return false;
            }
            return answers[id];
        }
    },
    getIAnswerById: function(id_question, id){
        var answers = App.cacheGet('answers');
        if(answers == undefined){
            return false;
        } else {
            try{
                answers = JSON.parse(answers);
            } catch (e){
                return false;
            }
            for(var i in answers[id_question]){
                if(answers[id_question][i].id == id){
                    return answers[id_question][i];
                }
            }

            return false;
        }
    }
};